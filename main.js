var canvas = document.getElementById("myCanvas");
var canvasContext = canvas.getContext("2d");

var width = 800;
var height = 800;

var img = canvasContext.createImageData(width, height);
noise.seed(Math.random());
for(var i = 0; i < img.data.length; i += 4)
{
	img.data[i+0] = 255;
	img.data[i+1] = 255;
	img.data[i+2] = 255;
	img.data[i+3] = Math.abs(PerlinNoise(Math.floor((i/4)/width), (i/4)%height, 0,  100, 127, 1));
}
canvasContext.putImageData(img, 0, 0);

function NoiseInt (x, y, scale, mag, exp)
{
	return Math.pow((noise.perlin2(x/scale, y/scale) + 1) * mag, exp);
}
function PerlinNoise(x, y, z, scale, height, power)
{
	var rValue;
	rValue = noise.perlin3(x/scale, y/scale, z/scale) + 1;
	rValue *= height;

	if(power != 0)
	{
		rValue = Math.pow(rValue, power);
	}

	return rValue;
}